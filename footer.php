
					<aside id="content-column-2" class="col-md-4">
						<?php get_sidebar('footer');?>
					</aside>
					<!--CONTENT END-->
				</div><!--row-->
				
				<div class="row">
					<nav id="page-nav" class="col-md-12">
						<ul class="pager">
							<li><?php echo previous_posts_link(); ?></li>
							<li><?php echo next_posts_link(); ?></li>
						</ul>
					</nav>
				</div>
			</div><!--container-->
		</div><!--content-->


		<footer id="footer">
			<div class="container centered">

			&copy; 2012 Greg Bueno
			</div>
		</footer>

		<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<script type="text/javascript">
		try {
			var pageTracker = _gat._getTracker("UA-7828220-7");
			pageTracker._trackPageview();
		} catch(err) {}
		</script>

		<?php wp_footer(); ?>

	</body>
</html>
